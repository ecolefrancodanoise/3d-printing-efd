module ProMiniHoles() {
    //Pro mini holes
    for (xPos = [0:11]) {
        for (yPos = [0, 2.54*6]) {
            translate([xPos*2.54-proMiniXOffset, yPos-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
        }
    }
    for (yPos = [0:3]) {
        translate([-proMiniXOffset, yPos*2.54-proMiniYOffset,0])  cube([2.7,2.7,plateHeight], center = true);
    }
    translate([-proMiniXOffset+2.54*6.5, 2.54-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
    translate([-proMiniXOffset+2.54*7.5, 2.54-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
    for (yPos = [1:6]) {
        translate([-proMiniXOffset+12*2.54, -2.54/2+yPos*2.54-proMiniYOffset,plateHeight/2]) cube([2.54,2.54,plateHeight], center = true);
    }
}