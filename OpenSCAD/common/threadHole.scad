module threadHole(baseRadius, height, degreesPerMm, tolerance)

linear_extrude(height,twist=height*degreesPerMm, center=true)
    translate([0,offset,0]) circle(baseRadius+tolerance, $fn=50);
