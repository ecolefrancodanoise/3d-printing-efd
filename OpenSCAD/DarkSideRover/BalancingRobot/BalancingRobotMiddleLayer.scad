include <config.scad>
include <threadHole.scad>
include <arduinoUnoHoles.scad>
$fn=60;
plateHeight=2;
tolerance = 0.04; //see testHole.scad
distance = 40;
difference() {
    cube([distance*3+20,75,plateHeight], center=true);
    mirror([0, 0, 1]) translate([0,20,0]) cylinder(r=baseRadius, h=1, $fn=3); //small direction marker
    for (xPos = [-45, -15, 15, 45]) { //unisynx holes
        for (yPos = [-15, 15]) { //unisynx holes
            mirror([0, 0, 1]) translate([xPos,yPos,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        }
    }
    for (xPos = [-distance-10, -distance, distance, distance + 10]) { //unisynx holes
        for (yPos = [distance/2+5, -distance/2-5]) { //unisynx holes
            mirror([0, 0, 1]) translate([xPos,yPos,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        }
    }
    translate([0,33,0]) cube([30, 3.5, plateHeight], center=true);
    translate([0,-33,0]) cube([30, 3.5, plateHeight], center=true);
    translate([-50,,0]) cylinder(r=5, h=plateHeight, center=true);
}
