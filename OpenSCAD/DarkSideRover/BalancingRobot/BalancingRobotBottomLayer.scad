include <config.scad>
include <threadHole.scad>
include <arduinoUnoHoles.scad>
$fn=60;
plateHeight=2;
tolerance = 0.04; //see testHole.scad
distance = 40;
difference() {
    cube([distance*3+20,75,plateHeight], center=true);
//    translate([-23, -25, 0]) arduinoUnoHoles(plateHeight, 1.5);
    translate([5+0, -5, 0]) cylinder(h=plateHeight, r=1.5,center=true); //motor controller
    translate([5+26, -5, 0]) cylinder(h=plateHeight, r=1.5,center=true);//motor controller
    translate([0,20,0]) cylinder(r=baseRadius, h=1, $fn=3); //small direction marker
    for (offset = [-distance:distance*2:distance]) { //unisynx holes
        translate([offset,distance/2+5,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        translate([offset,-distance/2-5,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        for (x = [-20:40:20]) {//Merkur holes
            for (y = [-20:40:20]) {
                translate([x + offset, y, 0] ) cylinder(r=2.0,h=plateHeight, center=true);
            }
        }
    }
}
