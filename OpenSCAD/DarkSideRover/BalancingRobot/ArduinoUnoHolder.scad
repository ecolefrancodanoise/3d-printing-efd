include <config.scad>
include <threadHole.scad>
include <arduinoUnoHoles.scad>
$fn=60;
plateHeight=2;
tolerance = 0.04; //see testHole.scad
holeRadius = 0.6;
arduinoHoleRadius = 1.5;
difference() {
    union() {
        cube([65,65,plateHeight], center=true);
        translate([-25,-25,plateHeight]) arduinoUnoHoles(plateHeight, arduinoHoleRadius*2);
    }
    translate([0,15,0]) cylinder(r=baseRadius, h=plateHeight/2, $fn=3); //small direction marker
    for (xPos = [-15, 15]) { //unisynx holes
        for (yPos = [-15, 15]) { //unisynx holes
            translate([xPos,yPos,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        }
    }
    translate([-25,-25,+plateHeight/2]) arduinoUnoHoles(plateHeight*2, arduinoHoleRadius);
}
