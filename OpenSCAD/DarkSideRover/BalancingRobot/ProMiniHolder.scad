include <config.scad>
include <threadHole.scad>
include <threadHole.scad>
include <pinHoleSquare.scad>

$fn=60;
plateHeight=2;
tolerance = 0.04; //see testHole.scad
distance = 40;
proMiniYOffset = 3*2.54;
proMiniXOffset = 8;
difference() {
    cube([55,55,plateHeight], center=true);
    translate([0,15,0]) cylinder(r=baseRadius, h=plateHeight/2, $fn=3); //small direction marker
    for (xPos = [-15, 15]) { //unisynx holes
        for (yPos = [-15, 15]) { //unisynx holes
            translate([xPos,yPos,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        }
    }
    //Pro mini holes
    for (xPos = [0:11]) {
        for (yPos = [0, 2.54*6]) {
            translate([xPos*2.54-proMiniXOffset, yPos-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
        }
    }
    for (yPos = [0:3]) {
        translate([-proMiniXOffset, yPos*2.54-proMiniYOffset,0])  cube([2.7,2.7,plateHeight], center = true);
    }
    translate([-proMiniXOffset+2.54*6.5, 2.54-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
    translate([-proMiniXOffset+2.54*7.5, 2.54-proMiniYOffset,0]) cube([2.7,2.7,plateHeight], center = true);
    for (yPos = [1:6]) {
        translate([-proMiniXOffset+12*2.54, -2.54/2+yPos*2.54-proMiniYOffset,plateHeight/2]) cube([2.54,2.54,plateHeight], center = true);
    }

    //camera & gyroscope
    for (yPos = [-24, 24]) {
        for (xPos = [0:7]) {
            translate([xPos*2.54-proMiniXOffset, yPos,0])  cube([2.7,2.7,plateHeight], center = true);
        }
    }
    //esp8266 or nrf24
    for (xPos = [0, 2.48]) {
        for (yPos = [0:3]) {
            translate([xPos-proMiniXOffset -7, 2.48*yPos-proMiniYOffset,0])  cube([2.48,2.48,plateHeight], center = true); //slightly smaller
        }
    }

}
