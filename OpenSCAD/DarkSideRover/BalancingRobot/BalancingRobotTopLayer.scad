include <config.scad>
include <threadHole.scad>
include <arduinoUnoHoles.scad>
$fn=60;
plateHeight=2;
tolerance = 0.04; //see testHole.scad
distance = 40;
intersection() {
union() {
difference() {
    cube([distance*3+20,75,plateHeight], center=true);
    translate([0,20,0]) cylinder(r=baseRadius, h=1, $fn=3); //small direction marker
    for (xPos = [-distance-10, -distance, distance, distance + 10]) { //unisynx holes
        translate([xPos,distance/2+5,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
        translate([xPos,-distance/2-5,0]) threadHole (baseRadius, plateHeight, degreesPerMm, tolerance);
    }
}
for (j = [-3*8, 3*8]) {
    difference() {
        translate([j, 0, plateHeight/2 + 9.6/2]) cube([8,40, 9.6], center=true);
        for(i = [-2:2]) {
            translate([j, 8*i, plateHeight/2 + 5.6]) rotate([0,90,0]) cylinder(h=8, d=4.8, center = true);
        }
    }
}

for(j = [0:8:(8*1)]){
    for(i = [-32:8:32]){
        translate([j+60-1.6,i,+plateHeight/2])cylinder(d=4.8+0.2,h=1.6+0.2);
        translate([j-68+1.6,i,+plateHeight/2])cylinder(d=4.8+0.2,h=1.6+0.2);
    }
}
}
translate([83,32,0]) cube([57,57,57], center = true);
}