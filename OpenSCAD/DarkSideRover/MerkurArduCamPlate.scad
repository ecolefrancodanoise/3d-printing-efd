//Arduino hole spacing from
//https://www.flickr.com/photos/johngineer/5484250200/sizes/o/in/photostream/
$fn=60;
thickness=1.75;
arduinoHoleRadius = 1.5;//1.3;
merkurHoleRadius = 2.0;//1.75;
union(){
    difference() {
        translate([10,-thickness,0]) cube([90,50,thickness]);
        union() {
            translate([14+1.3, 13, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
            translate([14+1.3,13+20, 0]) cylinder(h=thickness, r=arduinoHoleRadius);
            for (i=[20:10:90]) {
                for (j=[8:10:40]) {
                    translate([i, j, 0]) cylinder(h=thickness, r=merkurHoleRadius);
                }
            }
        }
    }
    translate([100, -thickness,0])rotate(-90, [0,1,0]){
    difference() {
        cube([30, 50, thickness]);
        union() {
            for (i=[10,10,20]) {
                for (j=[10:10:40]) {
                    translate([i, j, 0]) cylinder(h=thickness, r=merkurHoleRadius);
                }
            }
            }
        }
    }
}
