include <config.scad>

threadHeight = 18;
difference() {
    union() {
        linear_extrude(threadHeight,twist=threadHeight*degreesPerMm)
            translate([0,offset,0]) circle(baseRadius, $fn=50);
        mirror([0,0,1]) linear_extrude(threadHeight,twist=threadHeight*degreesPerMm)
            translate([0,offset,0]) circle(baseRadius, $fn=50);
    }

    cylinder(r=baseRadius / 2,h=threadHeight, $fn=6);
    translate([baseRadius/2,0,-threadHeight]) cylinder(r=baseRadius/5, h=2, $fn=3); //small direction marker
}
