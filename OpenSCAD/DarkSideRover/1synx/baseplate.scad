include <config.scad>
include <fourThreads.scad>
include <arduinoUnoHoles.scad>
include <servoHoles.scad>

plateHeight=2;
tolerance = 0.22; //0.2 sort of ok, 0.25 slightly unstable
difference() {
    cube([75,115,plateHeight], center=true);
    translate([0,35,0]) fourThreads(baseRadius, plateHeight, degreesPerMm);
    translate([0,-35,0]) fourThreads(baseRadius, plateHeight, degreesPerMm);
    translate([-25, -25, 0]) arduinoUnoHoles(plateHeight, 1.5);
    translate([0, -40, 0]) cylinder(h=plateHeight, r=1,5,center=true);
    translate([26, -40, 0]) cylinder(h=plateHeight, r=1.5,center=true);
    translate([22, 49, 0]) servoHoles(plateHeight);
    translate([-22, 49, 0]) servoHoles(plateHeight);
    translate([-22, -49, 0]) servoHoles(plateHeight);
    translate([20,0,0]) cylinder(r=baseRadius, h=1, $fn=3); //small direction marker
    cylinder(r=14, h=plateHeight, $fn=9, center=true); //remove unnecessary material
}
