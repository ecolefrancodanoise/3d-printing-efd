
module arduinoUnoHoles(thickness, arduinoHoleRadius) {
    translate([0   , 2.5       , 0]) cylinder(h=thickness, r=arduinoHoleRadius, center=true);
    translate([0   , 53.3 - 2.5, 0]) cylinder(h=thickness, r=arduinoHoleRadius, center=true);
    translate([50.8, 7.3       , 0]) cylinder(h=thickness, r=arduinoHoleRadius, center=true);
    translate([50.8, 7.3+27.9  , 0]) cylinder(h=thickness, r=arduinoHoleRadius, center=true);
}