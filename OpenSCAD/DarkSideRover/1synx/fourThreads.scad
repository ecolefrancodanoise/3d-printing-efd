module fourThreads(baseRadius, height, degreesPerMm)

for (i=[-5:10:5])
    for (j=[-5:10:5])
        translate([i,j,0])
            linear_extrude(height,twist=height*degreesPerMm, center=true)
                translate([0,offset,0]) circle(baseRadius+tolerance, $fn=50);
