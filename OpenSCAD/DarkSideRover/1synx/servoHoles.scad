
module servoHoles(height) {
    cube([24.5,12.6,height], center = true);
    translate([13.75,0,0]) cylinder(r=1,h=height, center = true, $fn=45);
    translate([-13.75,0,0]) cylinder(r=1,h=height,center = true, $fn=45);
}