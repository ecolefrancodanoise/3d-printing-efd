include <config.scad>
include <fourThreads.scad>
plateHeight=2;
tolerance = 0.22; //0.2 sort of ok, 0.25 slightly unstable
mirror([0,0,1])
    difference() {
        cube([35,35,plateHeight], center=true);
        fourThreads(baseRadius, plateHeight, degreesPerMm);
        for (i=[-15:30:15])
            for (j=[-15:30:15])
                translate([i, j, 0]) cylinder(r=1.70, h=plateHeight, $fn=50, center=true);

        translate([baseRadius+2,0,0]) cylinder(r=baseRadius/5, h=1, $fn=3); //small direction marker

    }
