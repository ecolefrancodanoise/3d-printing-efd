$fn=120;
plateHeight = 2;
difference() {
    cylinder(h=plateHeight,r=14);
    for (angle = [0:120:360]) {
        rotate([0, 0, angle]) translate([8,0,0]) cylinder(d = 3.5, h=plateHeight);
        rotate([0, 0, angle+60]) translate([10,0,0]) cylinder(d = 3.5, h=plateHeight);
    }
}