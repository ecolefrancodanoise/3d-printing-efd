wallThickness = 0.87;
interiorLength = 150;
interiorWidth = 36;
interiorHeight = 15;

difference() {
    translate([-wallThickness, -wallThickness, -wallThickness]) cube([interiorLength+2*wallThickness, interiorWidth+2*wallThickness, interiorHeight+wallThickness]);
    cube([interiorLength, interiorWidth, interiorHeight]);
    translate([5, -wallThickness, interiorHeight - 5]) rotate([-90, 0, 0]) cylinder(interiorWidth+2*wallThickness, d=3.2);
    translate([interiorLength-5, -wallThickness, interiorHeight - 5]) rotate([-90, 0, 0]) cylinder(interiorWidth+2*wallThickness, d=3.2);
}