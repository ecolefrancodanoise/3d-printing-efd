N = [3, 7, 10, 6];
difference() {
    for (i = [0:len(N)-1]) {
        for (j = [0:N[i]-1]) {
            rotate([0, 0, j * 360/N[i]]) translate([N[i], 0, i * 5]) sphere(d = 10, $fn=50);
        }
    }
    cylinder(h=50, d=2, center = true);
}