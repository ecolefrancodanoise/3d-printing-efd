include <3030Nut.scad>

difference() {
    intersection () {
        union() {
            translate([0,0,2]) translate() 3030Nut();
            translate([2,0,0]) scale([1,1,-1]) rotate([0, 90, 0]) 3030Nut();
            translate([-2.5, 0, -2.5])  cube([7+2, 16.4 * 0.945, 7+2],center = true);
        }
        translate([33, 0, 33]) rotate([0, -45, 0]) rotate([90, 0, 0]) rotate([0, 45, 0]) cube([80,80,80], center = true);
    }
    translate ([0, 3.0, 0]) rotate([0,45, 0]) cylinder(h=20, d=1.6, center = true);
    translate ([0, -3.0, 0]) rotate([0,45, 0]) cylinder(h=20, d=1.6, center = true);
    translate ([0, 0, 20 + 2]) rotate([0,90, 0]) cylinder(h=10, d=2, center = true);
    translate ([20 + 2, 0, 0]) cylinder(h=10, d=2, center = true);
}
