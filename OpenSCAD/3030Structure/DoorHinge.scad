include <3030Nut.scad>

include <3030Nut.scad>


windowThickness = 3.0;
slotThickness = 6.5;
intersection() {
difference() {
    scale([1,1,0.6]) union() {
        3030Nut();
        translate([0, -slotThickness/2, 0]) cube([7,slotThickness,50]);
    }
    #translate([-6, 1, 2.5]) cube([12+1, 1 + 0.2, 25 + 0.7]);
    #translate([-6, 1, 2.5]) cylinder(h=25 + 0.7, d=4.5+0.5);
}
    cube([20, 20, 20], center = true);
}