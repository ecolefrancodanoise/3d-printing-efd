module edgeDodecahedron (isOdd, skewX, skewY, nbTeeth=5) {
    isNegative = isOdd;
    tolerance = .10;
    h2 = 150;
    a = 100;
    r1 = a / 2 / sin(72/2);
    //h3 = sqrt(1/2 - (cos(30)/2)^2); //height of the face (r of circumscribed circle is a/sqrt(2))
    toothWidth= (a-a*2/10) / 2 / nbTeeth*1.071; //tooth take up 80% of border width
    b = a / 2 / tan(36); //distance from face center to middle of face edge
    h3 = 1.113;
    skewMat =   [ [ 1, skewX, skewY,   0],
                  [ 0,     1, 0,   0],
                  [ 0,     0,     1,   0],
                  [ 0,     0,     0,   1]];
    difference () {
        union() {
            rotate([0,0,90]) cylinder(r1=r1, r2=0, h=a*h3, $fn=5);
            if (!isNegative) {
                for (i = [0:nbTeeth-1]) {
                    translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth-42.8, -b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,8,14.15]);
                }
            }
        }
        translate([0, 10,0]) rotate([0,0,90]) cylinder(r1=r1, r2=r1, h=a*h3, $fn=5);
       translate([85, 0, 0]) rotate([0,0,-36]) cylinder(r1=r1, r2=r1, h=a*h3, $fn=5);
         translate([-85, 0, 0]) rotate([0,0,72]) cylinder(r1=r1, r2=r1, h=a*h3, $fn=5);
        if (isNegative) {
                for (i = [0:nbTeeth-1]) {
                    #translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth-42.8, -b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,8,14.15]);
            }
        }
    }
}

//edgeDodecahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=6);