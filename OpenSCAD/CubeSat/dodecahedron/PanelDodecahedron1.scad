include <EdgeDodecahedron.scad>

module panelDodecahedron1() {
    rotate([0, 0, 0])  edgeDodecahedron(isOdd = 1, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 72]) edgeDodecahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 144]) edgeDodecahedron(isOdd = 1, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 216]) edgeDodecahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 288]) edgeDodecahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
}

panelDodecahedron1();