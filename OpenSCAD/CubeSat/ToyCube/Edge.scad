
module edge (isOdd, skew, relativeBorderWidth=0.1, faceThickness = 2.0, nbTeeth=5) {
    tolerance = .30;
    bw = 100 * relativeBorderWidth; //border width
    xor = (skew < 0 && isOdd ) || (skew > 0 && !isOdd) ? 1 : 0;
    skewX = xor? skew : 0;
    skewY = xor? 0 : -skew;
    isSkewNeg = skew < 0? 1 : 0;
    toothWidth= (100-2*bw) / 2 / nbTeeth;
    skewMat =   [ [ 1, skewX, skewY, 0],
                  [ 0,     1,     0, 0],
                  [ 0,     0,     1, 0],
                  [ 0,     0,     0, 1]];
    for (i = [-1:nbTeeth]) {
        translate([i*toothWidth*2-tolerance/2+isSkewNeg*toothWidth-50+bw*(1-skewX-skewY) + (skewX+skewY)*faceThickness, -50+faceThickness, faceThickness]) multmatrix(skewMat) cube([toothWidth-tolerance,bw-faceThickness,bw-faceThickness]);
    }

}
