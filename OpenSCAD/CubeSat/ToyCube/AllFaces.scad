include <Face.scad>
include <CubeConfig1.scad> //choose angle configuration file

relativeBorderWidth=0.181;
bw = 100 * relativeBorderWidth;
faceThickness = 2.0;
innerWidth = 100 * (1 - 2*relativeBorderWidth);
for (i = [1]) { //0:1
for (j = [0:1]) { //0:2
    index = 3*i+j;
    translate([110*i, 110*j, 0])
    difference() {
        union() {
        face(relativeBorderWidth=relativeBorderWidth, nbTeeth=2, index = index,
            translations=translations, normals=normals,
            neighbors=neighbors, parity=parities[index]);
            translate([-innerWidth/2,-innerWidth/2,0]) cube([innerWidth,innerWidth,bw]);
        }
        translate([-30,-30,0]) cube([60,60,bw]);
    }
}
}