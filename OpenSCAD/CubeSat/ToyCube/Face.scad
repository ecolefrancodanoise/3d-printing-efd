include <Edge.scad>

module face(relativeBorderWidth=0.1, faceThickness=2.0, nbTeeth=5, index, translations, normals, neighbors, parity) {
        bw = 100 * relativeBorderWidth; //border width
        toothWidth= (100-2*bw) / 2 / nbTeeth;

    difference() {
        rotate([0,0,45])  cylinder(r1=141.4/2, r2=0, h=50, $fn=4);
        translate([0, 0, faceThickness]) rotate([0,0,45]) cylinder(r1=141.4/2, r2=0, h=50, $fn=4);
    }
    intersection() {
        translate([-50+faceThickness, -50+faceThickness, faceThickness]) cube([100-2*faceThickness,100-2*faceThickness,100]);
        difference(){
            for (i = [0:3]) {
                neighbor = neighbors[index][i];
                skew = -(translations[index] - translations[neighbor]) * (cross(normals[index], normals[neighbor]));
                isOdd = parity[i];
                diff = translations[index] - translations[neighbor];
                rotate([0,0,i*90]) edge(isOdd = isOdd, skew = skew,
                    relativeBorderWidth = relativeBorderWidth, faceThickness = faceThickness, nbTeeth = nbTeeth);
            }
            translate([50-bw, 50-bw, faceThickness]) cube([bw, bw, bw]);
            translate([-50, -50, faceThickness]) cube([bw, bw, bw]);
            translate([50-bw, -50, faceThickness]) cube([bw, bw, bw]);
            translate([-50, 50-bw, faceThickness]) cube([bw, bw, bw]);
        }
    }
}
