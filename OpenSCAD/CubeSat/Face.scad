include <Edge.scad>

module face(relativeBorderWidth=0.1, faceThickness=2.0, tolerance = 0.1,
nbTeeth=5, index, translations, normals, neighbors, parity) {
        bw = 100 * relativeBorderWidth; //border width
        toothWidth= (100-2*bw) / 2 / nbTeeth;

    difference() {
        translate([-50, -50, 0]) cube([100,100,bw]);
        for (i = [0:3]) {
            neighbor = neighbors[index][i];
            skew = -(translations[index] - translations[neighbor]) * (cross(normals[index], normals[neighbor]));
            isOdd = parity[i];
            diff = translations[index] - translations[neighbor];
            rotate([0,0,i*90]) edge(isOdd = isOdd, skew = skew,
                relativeBorderWidth = relativeBorderWidth, faceThickness =            faceThickness, tolerance = tolerance, nbTeeth = nbTeeth);
        }
        translate([-50, -50, 0]) cube([bw, bw, bw]);
        translate([50-bw, -50, 0]) cube([bw, bw, bw]);
        translate([-50, 50-bw, 0]) cube([bw, bw, bw]);
        translate([50-bw, 50-bw, 0]) cube([bw, bw, bw]);
        translate([-(nbTeeth-1)*bw, -(nbTeeth-1)*bw, 0]) cube([2*(nbTeeth-1)*bw, 2*(nbTeeth-1)*bw, bw]);

    }
}
