include <LadderStep.scad>

module Ladder() {
scale([1.03, 1.03, 1.03]) //tolerance
difference() {
    translate([-15, -15, -15/2]) cube([15, 100, 15]);
    for (y = [0, 50]) {
    #translate([0,   y,  2]) LadderStep();
    #translate([0, 20+ y, -2]) rotate([180,0,0]) LadderStep();
    }
}
}

scale([1, 1, -1]) Ladder();