module Handle() {
    tol = 0.7;
    rotate([0,0,45]) {
        difference() {
            cylinder(h=20, r=25);
            cylinder(h=20, r=10);
            rotate([0, 0, 0]) cube([25, 25, 100]);
            rotate([0, 0, (240-90)/2]) cube([25, 25, 100]);
            rotate([0, 0, -(240-90)/2]) cube([25, 25, 100]);
            for (z=[1]) {
                rotate([0,0,-164]) translate([0, 10+(25-10)/2-8/2, z]) cube([8,8,8]);
            }
            for (z=[11]) {
                rotate([0,0,75]) translate([-8+0.5, 10+(25-10)/2-8/2+tol/2, z]) cube([8,8,8]);
            }
            translate([-26, -26, 50]) rotate([0,0,45]) cube([35, 35, 100], center=true);
        }

        for (z=[1]) {
            rotate([0,0,75+tol]) translate([0, 10+(25-10)/2-8/2+tol/2, z]) cube([8-tol,8-tol,8-tol]);
        }
        for (z=[11]) {
            rotate([0,0,-164-tol]) translate([-7.5, 10+(25-10)/2-8/2+tol/2, z]) cube([8-tol,8-tol,8-tol]);
        }
    }
}
module ThreeHandle() {
    translate([0, 19.65, 0]) rotate([-54.74,0,0]) union() {
            Handle();
            translate([0,0,20]) Handle();
            translate([0,0,40]) Handle();
        }
}


//experiment with this to make a corner:
//translate([0, 0, 10.15]) rotate([0,0,30]) cylinder(h=5.85, r1=25, r2=19, $fn=3);
//ThreeHandle();
//rotate([0, 0, 120]) ThreeHandle();
//rotate([0, 0, 240]) ThreeHandle();

Handle();