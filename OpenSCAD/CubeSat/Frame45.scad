tol = 0.2;
thickness = 2.6;

module traces() {
    for (i = [-7:7]) {
        traceDepth=0.4;
        translate([i*2.54, 0, thickness/2 - traceDepth]) rotate([90, 0, 0]) cylinder(h=40, r=traceDepth, center = true);
        translate([i*2.54, 0, -thickness/2 + traceDepth]) rotate([90, 0, 0]) cylinder(h=40, r=traceDepth, center = true);
    }

}

module connectorPlate(inclination, parity, connectorThickness = 2, withTraces = 1) {
    translate([-21.5, parity * (-44.5 + thickness), 0]) rotate([-45 * inclination, 0, 0])
    union() {
        difference() {
            cube([43 - thickness - 2 * tol,40, connectorThickness-tol], center = true);
            if (withTraces == 1) {
                traces();
            }
        }
        if (withTraces == -1) {
            traces();
        }
    }
}

intersection() {
    difference() {
        union() {
            hull() {
                cube([100-thickness*2, 100-thickness*2, thickness], center = true);
                translate([0, 0, -thickness/2]) cube([100, 100, 0.0001], center = true);
            }
            for (i = [0, 90, 180, 270]) {
                rotate([0,0,i]) connectorPlate(inclination = 1, parity = 1, withTraces=1);
                intersection() {
               rotate([0,0,i]) connectorPlate(inclination = -1, parity = -1, connectorThickness = 6, withTraces = 0);
                translate([0, 0, thickness/2]) cube([100-2*thickness, 100-2*thickness, 2*thickness], center = true);
                }
            }
        }
        for (i = [0, 90, 180, 270]) {
            rotate([0,0,i]) connectorPlate(inclination = -1, parity = -1, withTraces=-1);
        }
        cube([70, 70, thickness], center = true);
    }
    translate([0, 0, 30/2-1]) cube([100, 100, 30], center = true);
}