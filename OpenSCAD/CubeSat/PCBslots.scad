module pcbSlots() {
    tolerance = .10;
    slotHeight = 1.57+0.13+tolerance;
    slotDepth = 3;
    for (i = [-25:25:25]) {
        translate([i + -slotHeight/2,-40-slotDepth,10-slotDepth]) cube([slotHeight, 80+2*slotDepth, 80+2*slotDepth]);
        rotate([0, 0, 90]) translate([i + -slotHeight/2,-40-slotDepth,10-slotDepth]) cube([slotHeight, 80+2*slotDepth, 80+2*slotDepth]);
    }
}