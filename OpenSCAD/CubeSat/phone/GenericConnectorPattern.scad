
module toothShape(width, thickness, depth, roundness1, roundness2, barb) {
//        scale ([1,1,-1]) rotate([0,90,0]) scale([1,-1,1]) rotate([90,0,0]) linear_extrude(thickness) polygon([[0, 0], [depth,0], [depth,width], [0,width]]);
        scale ([1,1,-1]) rotate([0,90,0]) scale([1,-1,1]) rotate([90,0,0]) linear_extrude(thickness) polygon([[0, -roundness1] , [roundness1, 0], [depth-roundness2,0 - barb], [depth, roundness2], [depth, width-roundness2], [depth-roundness2,width+barb], [roundness1, width], [0, width+roundness1]]);
//        cube([width, thickness, depth]);
}

module GenericConnectorPattern(tol = 0.3, thickness = 2.0, lscale = 1.0, hscale=2.0, toothDepth=4, offset= 0.0, hoffset=0.0, separation = 1, separation2 = 1, nbTeeth = 8, toothRoundness1=0.8, toothRoundness2=0.8, angularDrift = 0.0, barb = 0) {
    skewMats = [
    [[ 1, -1, 0, 0],
    [ 0, 1, 0, 0],
    [ 0, 0, 1, 0],
    [ 0, 0, 0, 1],
    ],
    [[ 1, 1, 0, 0],
    [ 0, 1, 0, 0],
    [ 0, 0, 1, 0],
    [ 0, 0, 0, 1],
    ]
    ];
    scale([1,1,lscale]) for (j = [0:nbTeeth-1]) {
        rotate([0,90,0]) translate([j*thickness*2*separation*hscale + hoffset, offset,0]) {
            if (j % 2 == 0) {
                translate([0, 0, -abs(sin(angularDrift*j))*hscale*thickness]) rotate([0,angularDrift*j,0]) translate([tol/2,0,0]) multmatrix(skewMats[0]) toothShape(hscale*thickness-tol, thickness, toothDepth, roundness1=toothRoundness1, roundness2=toothRoundness2, barb = barb);
            } else {
                translate([0, 0, -abs(sin(angularDrift*j))*hscale*thickness]) rotate([0,angularDrift*j,0]) translate([separation2+tol/2,0,0]) multmatrix(skewMats[1]) toothShape(hscale*thickness-tol, thickness, toothDepth, roundness1=toothRoundness1, roundness2=toothRoundness2, barb = barb);
            }
        }
    }
}

//GenericConnectorPattern(thickness = 3, lscale = 1.8, hscale=1.3, barb = 0.1);
//GenericConnectorPattern(thickness = 2, lscale = 1.8, hscale=1.3);
//GenericConnectorPattern(thickness = 1, lscale = 3.6, hscale=1.3);
//GenericConnectorPattern(thickness = 1.4, lscale = 2, hscale=1.6);
//GenericConnectorPattern(thickness = 1.4, lscale = 2, hscale=2, separation = 1.5);
//GenericConnectorPattern(thickness = 1.5, lscale = 1.0, hscale=1.2, separation = 0.80, separation2=1.3);
//GenericConnectorPattern(thickness = 3, lscale = 1.8, hscale=1.3, nbTeeth=2);
