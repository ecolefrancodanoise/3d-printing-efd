//official
height = 150.5;
width =  75.8;
thickness=8.7;
dhmeasured = 6.1;
dvmeasured = 5.2;
lh = 26.0;
lv = 29.5;
dh = -width/2 + lh/2 + dhmeasured;
dv = -height/2 + lv/2 + dvmeasured;
lensHole = [-dv, -dh, lv, lh];
cornerOffset = 57; cornerRoundness = 4.5; roundness = 2.05;
angleOffsets = [60, 190, 325];
fingerprintHole = false;
nbTeeth = [3,5,3];
lscale = 1.07; hscale=3.6;
name = "IP11v05";

module negativeCorrections() {}

////iphone 13
//height = 146.7;
////width =  71.5;
////thickness=7.65;
////measured:
////height = 146.9;
//width =  71.8; //was 72, changed to 71.8
//thickness=8.65;
//cornerOffset = 57; cornerRoundness = 4.5; roundness = 0.05;
//lensHole = [55.5, 18.3, 24.0, 23.5];
//angleOffsets = [60, 190, 325];
//fingerprintHole = false;
//nbTeeth = [3,5,3];
//lscale = 1.07; hscale=3.6;
