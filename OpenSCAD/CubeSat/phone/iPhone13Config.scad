//official:
height = 146.8;
//width =  71.5;
//thickness=7.65;
//measured:
//height = 146.9;
width =  71.8; //was 72, changed to 71.8
thickness=8.65+1;//due to filament falling
cornerOffset = 57; cornerRoundness = 4.5; roundness = 0.05;
lensHole = [55.5, 18.3, 24.0, 23.5];
angleOffsets = [60, 190, 325];
fingerprintHole = false;
nbTeeth = [3,5,3];
lscale = 1.07; hscale=3.6;
name = "IP13";
module negativeCorrections() {}
