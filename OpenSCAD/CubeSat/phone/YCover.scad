//include <iPhone14Config.scad>
//include <iPhone14PlusConfig.scad>
//include <iPhone13Config.scad>
//include <iPhone12Config.scad>
//include <iPhone12MiniConfig.scad>
//include <iPhone11Config.scad>
include <MotoG8Config.scad>
//include <MotoOneActionConfig.scad>
//include <SamsungA14Config.scad>
//include <SamsungA34Config.scad>
//include <SamsungA25Config.scad>
//include <OnePlusConfig.scad>

//include <PCConfig.scad>
//include <PCConfigCarbon.scad>
//include <PLAConfig.scad>
include <NoConfig.scad>

include <PhoneCoverPieceWithoutTeeth.scad>
include <GenericConnectorPattern.scad>

shellThickness = 1.5;
toothDepth = 9;
d = 5.8;

module connectorZone(index, angleOffsets, nbTeeth, lscale, hscale, tol, toothRoundness1, toothRoundness2) {
    rotate([0,-angleOffsets[index]-90,0]) scale([-1, 1, 1]) 
        GenericConnectorPattern(tol = tol, thickness = shellThickness, lscale = lscale, hscale=hscale, toothDepth=toothDepth, offset = -thickness/2-shellThickness, hoffset=3.5, separation = 1.07, separation2=-2, nbTeeth = nbTeeth[index], toothRoundness1 = toothRoundness1, toothRoundness2 = toothRoundness2, angularDrift=2.4, barb = 0.3;
}

module YCover(index, angleOffsets) {
    difference() {
        PhoneCoverPieceWithoutTeeth(index, shellThickness = shellThickness, angleOffsets=angleOffsets);
        connectorZone((index+1)%3, angleOffsets=angleOffsets, nbTeeth = nbTeeth, lscale = lscale, hscale=hscale, tol = -tolerance, toothRoundness1=0.7, toothRoundness2=0.0);
    }
    intersection() {
        connectorZone(index, angleOffsets = angleOffsets, nbTeeth = nbTeeth, lscale = lscale, hscale=hscale, tol = tolerance, toothRoundness1=0.0, toothRoundness2=0.7);
        PhoneCoverPieceWithoutTeeth((index+2)%3, shellThickness = shellThickness, angleOffsets=angleOffsets);
    }
}

difference() {
    scale(shrinkFactor)
    for (i=[0:2]) {
        angle = angleOffsets[i] < angleOffsets[(i+1)%3]? (angleOffsets[i] + angleOffsets[(i+1)%3])/2 : (angleOffsets[i] + angleOffsets[(i+1)%3] +360)/2 ;
        translate([cos(angle)*d, 0, sin(angle)*d]) YCover(index = i, angleOffsets=angleOffsets);
    }
//#translate([88,-5,-17]) cube([8,8,8]);
//#translate([-height/2 + 2.5,-thickness/2-0.8, 20]) scale(0.1) rotate([90, 0,0]) linear_extrude(100) import("esa-logo.svg");
//#translate([-height/2 + 2.5,-thickness/2-0.8, 12]) rotate([90, 0,0])  linear_extrude(10) scale(0.7) text("Edition");
translate([-height/2 + 2.5,-thickness/2-0.8, 26]) rotate([90, 0,0])  linear_extrude(10) scale(0.7) text("Concours Lépine");
translate([-height/2 + 2.5,-thickness/2-0.8, 16]) rotate([90, 0,0])  linear_extrude(10) scale(0.7) text("2024");
#translate([-height/2 + 2.5,-thickness/2+1.0, -28]) rotate([90, 0,0])  linear_extrude(2) scale(0.7) scale([1, -1, 1]) scale(0.7) text(name);
}