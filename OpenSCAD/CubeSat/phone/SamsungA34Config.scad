//Official: 167.7 x 78 x 9.1 mm

height = 160.0; //trial an error 161.3 L
width =  77.5; //trial an error 77.5
thickness = 9.2; //trial and error, was 9.0
cornerOffset = 61;
roundness = 2; cornerRoundness = 3;
lensHole = [56-2.6, 22, 36, 15]; angleOffsets = [64, 188, 326];
fingerprintHole = false;
nbTeeth = [3,6,4];
lscale = 1.05; hscale=3.4;

module negativeCorrections() {}