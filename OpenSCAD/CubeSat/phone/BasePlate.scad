
module basePlate(height, width, shellThickness) {
    rotate([90, 0, 0])
    intersection() {
        hull() {
                r = shellThickness;
                for (i = [-height/2+r, height/2-r]) {
                    for (j = [-width/2+r, width/2-r]) {
                        translate([i,j,0]) cylinder(h=shellThickness, r1=r, r2=r/2, $fn=20);
                    }
                }
            }
        minkowski() {
            cube([height-20, width-20, shellThickness], center = true);
            cylinder(h=10, d=20, center = true);
        }
    }
}

//basePlate(height = 160, width = 60, shellThickness = 1.5);