$fn = 20;
cornerSize = 25;

module BaseShapeG8Inner(r, r2, thickness) {
    minkowski() {
        cube([2*(cornerSize-r2),thickness,2*(cornerSize-r2)], center = true);
        rotate([90, 0, 0]) cylinder(r=r2, h=0.0001);
    }
}

module BaseShapeG8(r, r2, thickness) {
    minkowski() {
        BaseShapeG8Inner(r = r,r2 = r2, thickness=thickness-2*r);
        sphere(r=r);
    }
}

module caseCorner (thickness, roundness, cornerRoundness, shellThickness) {
r = roundness; r2 = cornerRoundness;
    translate([-roundness, 0, -roundness])
    translate([0,-0.01,0]) //somehow needed (vertical alignment)
    intersection() {
    difference() {
         difference() {
            minkowski() {
                BaseShapeG8(r = roundness,r2 = cornerRoundness, thickness=thickness);
                sphere(r = shellThickness);
            }
            cornerStuffing = 3.0;
            BaseShapeG8(r = roundness, r2 = cornerRoundness+cornerStuffing, thickness=thickness);
        }
        translate([-2, 23, 0]) rotate([0,-45,0]) cylinder(r=35, h=100, center = true, $fn=40);
        translate([-15, 0, -15]) cube([50, 50, 50], center = true);
    }
    translate([15.5, 0, 15.5]) rotate([0, 45,0]) scale([1,1,1]) sphere(r=11.5+roundness+cornerRoundness, $fn=40);
    }
}

//caseCorner(thickness = 9+0.5, roundness =4, cornerRoundness = 4, shellThickness = 1.5);

//caseCorner(thickness = 9+0.5, roundness =0.75, cornerRoundness = 3.5, shellThickness = 1.5);