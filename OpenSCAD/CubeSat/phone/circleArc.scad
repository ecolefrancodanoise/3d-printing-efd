module circleArc(angle1, angle2, stepSize = 1) {
        scale(200) translate([0,0,-0.5]) linear_extrude(1) polygon ([[0,0], [cos(angle1),sin(angle1)], for (j = [angle1:stepSize:(angle2 >= angle1 ? angle2 : angle2 + 360)]) [cos(j), sin(j)] ]);
}
