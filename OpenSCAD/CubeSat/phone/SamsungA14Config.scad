//Official: 167.7 x 78 x 9.1 mm

//height = 166.0; //not measured, trial and error
height = 167.0; //not measured, trial and error, 167.7, 167.2
width =  77.7; //measured 77.9, trial and error 
thickness = 9.3; //measured
cornerOffset = 61;
roundness = 2; cornerRoundness = 3; 
lensHole = [56, 22, 36, 15]; angleOffsets = [64, 188, 326];
fingerprintHole = false;
nbTeeth = [3,6,4];
lscale = 1.05; hscale=3.4;

module negativeCorrections() {
    translate ([-84,-1,-19]) cube([7,7,7], center = true);
}
