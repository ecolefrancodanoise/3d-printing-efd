include <circleArc.scad>
include <BasePlate.scad>
include <CaseCorner.scad>

$fn=20;
module placeCornerUpperRight(shellThickness) {
    translate([height/2-22.90-shellThickness, -0.02, width/2-22.90-shellThickness]) caseCorner(thickness = thickness, roundness = roundness, cornerRoundness = cornerRoundness, shellThickness = shellThickness);
}

module PhoneCoverPieceWithoutTeeth(index, angleOffsets, shellThickness = 2.0) {
    intersection() {
            difference() {
                union() {
                    translate([0, -thickness/2, 0]) basePlate(height = height, width = width, shellThickness = shellThickness);
                    placeCornerUpperRight(shellThickness);
                    scale([1,1,-1]) placeCornerUpperRight(shellThickness);
                    scale([-1,1,-1]) placeCornerUpperRight(shellThickness);
                    scale([-1,1,1]) placeCornerUpperRight(shellThickness);
                }
                translate([lensHole[0], -6, lensHole[1]]) minkowski() {
                    cube([lensHole[2]-8, 5,lensHole[3]-8], center = true);
                    sphere(r=4);
                }
//                nameDepth = 0.7;
//                translate([-70, -thickness/2 + -shellThickness+nameDepth, 18]) rotate([90, 0, 0]) linear_extrude(nameDepth) text(name);
            if (fingerprintHole) {
                translate([31, 0, 0]) rotate([90, 0, 0]) cylinder (r1=6, r2=11, h = 10, $fn=30);
            }
            negativeCorrections();
            }
            rotate([90,0,0]) circleArc(angleOffsets[index], angleOffsets[(index+1)%3]);
    }
}
//include <iPhone12MiniConfig.scad>
//PhoneCoverPieceWithoutTeeth(index = 0, angleOffsets = [60, 180, 300]);
//include <iPhone11Config.scad>
//PhoneCoverPieceWithoutTeeth(index = 0, angleOffsets = [60, 180, 300]);
//PhoneCoverPieceWithoutTeeth(index = 1, angleOffsets = [60, 180, 300]);
//PhoneCoverPieceWithoutTeeth(index = 2, angleOffsets = [60, 180, 300]);