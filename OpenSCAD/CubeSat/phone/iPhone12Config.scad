height = 146.9;
width =  71.5;
thickness=7.4;
cornerOffset = 57; cornerRoundness = 3.5; roundness = 0.05; 
lensHole = [53.5, 16.5, 22.5+8, 22.5+8];
angleOffsets = [60, 190, 325]; 
fingerprintHole = false;
nbTeeth = [3,5,3];
lscale = 1.07; hscale=3.6;

module negativeCorrections() {}
name="IP12v06";