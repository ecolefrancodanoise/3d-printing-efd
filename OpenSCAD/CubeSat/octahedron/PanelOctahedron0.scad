include <EdgeOctahedron.scad>

module panelOctahedron0() {
    rotate([0, 0, 0])  edgeOctahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 120]) edgeOctahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 240]) edgeOctahedron(isOdd = 1, skewX = 0, skewY = 0, nbTeeth=5);
}

panelOctahedron0();