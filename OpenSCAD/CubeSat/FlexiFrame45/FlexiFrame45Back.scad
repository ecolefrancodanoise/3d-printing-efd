include <FlexiFrameConfig.scad>

difference() {
    cube([128, 128, 2], center = true);
    #for (i = [0:3]) {
        rotate([0, 0, 90*i]) translate ([0, 60, 2]) rotate([-45, 0, 0])
        for(j = [0:7] ) {
            translate([-j*4-16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
            translate([ j*4+16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
        }
    }
    translate([0, 0, 0.25]) cylinder(d=91, h=1.5, center = true, $fn = 60);
    translate([0, -35, 0]) cylinder(d=22, h=2, center = true, $fn = 60);
    translate([46-19, 0, 0]) cube([2,25,2], center = true);
    translate([-46+13.5, 7, 0]) cube([2,8,2], center = true);
}