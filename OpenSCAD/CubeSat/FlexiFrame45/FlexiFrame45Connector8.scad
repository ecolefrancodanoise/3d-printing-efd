include <FlexiFrameConfig.scad>

for(j = [0:7] ) {
    translate([j*4, 0, 0]) cube([2-tol, 2-tol, 15], center = true);
}

translate([14, 0, 0])
//union() {
hull() {
    translate([0, -1+0.1/2+tol/2, 0]) cube([28, 0.1, 4], center = true);
    translate([0,  1-0.1/2-tol/2, 0]) cube([28, 0.1, 8], center = true);
}