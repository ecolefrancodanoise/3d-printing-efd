include <FlexiFrameConfig.scad>

N = 4;
for(j = [0:N-1] ) {
    translate([j*4, 0, 0]) cube([2-tol, 2-tol, 15], center = true);
}

translate([2 * (N-1), 0, 0])
//union() {
hull() {
    translate([0, -1+0.1/2+tol/2, 0]) cube([4*(N-1), 0.1, 4], center = true);
    translate([0,  1-0.1/2-tol/2, 0]) cube([4*(N-1), 0.1, 8], center = true);
}