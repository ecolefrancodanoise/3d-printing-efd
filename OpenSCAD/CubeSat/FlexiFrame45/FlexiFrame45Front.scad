include <FlexiFrameConfig.scad>

difference() {
    cube([128, 128, 2], center = true);
    #for (i = [0:3]) {
        rotate([0, 0, 90*i]) translate ([0, 60, 2]) rotate([-45, 0, 0])
        for(j = [0:7] ) {
            translate([-j*4-16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
            translate([ j*4+16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
        }
    }
    intersection() {
        cylinder(d=50, h=2, center = true, $fn = 60);
        for (i = [-6:6]) {
            #translate([0, i*4, 0]) cube([50, 2, 3], center = true);
        }
    }
}
