include <FlexiFrameConfig.scad>


difference() {
    cube([128, 32, 2], center = true);
    #for (i = [0,2]) {
        rotate([0, 0, 90*i]) translate ([0, 60-(96/2), 2]) rotate([-45, 0, 0])
        for(j = [0:7] ) {
            translate([-j*4-16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
            translate([ j*4+16, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
        }
    }
    #for (i = [1,3]) {
        rotate([0, 0, 90*i]) translate ([0, 60, 2]) rotate([-45, 0, 0])
        for(j = [0:1] ) {
            translate([-j*4-2, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
            translate([ j*4+2, 0, 0]) cube([2+tol, 2+tol, 10], center = true);
        }
    }
}
