include <BOSL2/std.scad>
include <BOSL2/screws.scad>
include <Frame254/Frame254mmBase.scad>

actuatorLength = 100.0;
screwSpec = "M10";
gearBlockSize = 100;
displacement = 70;

module halfNut () {
translate([0, 0, actuatorLength/2]) screw(spec = screwSpec, length=actuatorLength, bevel1=false, bevel2=false, blunt_start = false, head="none", $fn=40, thread=true, drive = "none");
}

module linearActuator (rotationDir = 1) {
    rotate([0, 0, 45])
    translate ([0,0, actuatorLength]) union() {
        halfNut();
        scale([1, 1, -1]) halfNut();
        translate([20, 0, 0]) cylinder(h=actuatorLength * 2, d = 10, center = true);
        translate([-20, 0, 0]) cylinder(h=actuatorLength * 2 , d = 10, center = true);
        translate([75, 0, displacement]) cube([250, 20, 2], center = true);
        translate([75, 0, -displacement]) cube([250, 20, 2], center = true);
        translate([210, 0, -displacement+2]) rotate([0,0, rotationDir * 45]) Frame254mmBase();
        translate([210, 0, displacement-2]) rotate([0,0, rotationDir * 45]) rotate([180,0,0])Frame254mmBase();
    }
}

translate([0, 0, gearBlockSize / 2]) linearActuator(rotationDir = -1);
translate([0, actuatorLength*2 + gearBlockSize/2, 0]) rotate([90, 0, 0]) linearActuator();
translate([actuatorLength*2 + gearBlockSize/2, 0, 0]) rotate([0, -90, 0]) linearActuator();

cube([gearBlockSize, gearBlockSize, gearBlockSize], center = true);