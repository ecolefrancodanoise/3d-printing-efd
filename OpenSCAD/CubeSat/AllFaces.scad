include <Face.scad>
include <CubeConfig1.scad> //choose angle configuration file

nbTeeth=15;
relativeBorderWidth = 1/(2*nbTeeth+2);
bw = 100 * relativeBorderWidth;
faceThickness = 0.0;
innerWidth = 100 * (1 - 2*relativeBorderWidth);
for (i = [0:1]) {
for (j = [0:2]) {
    index = 3*i+j;
    translate([110*i, 110*j, 0])
        face(relativeBorderWidth=relativeBorderWidth,
            faceThickness = faceThickness, tolerance = 0.3, nbTeeth=nbTeeth, index = index,
            translations=translations, normals=normals,
            neighbors=neighbors, parity=parities[index]);
}
}
