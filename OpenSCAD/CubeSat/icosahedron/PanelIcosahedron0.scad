include <EdgeIcosahedron.scad>

module panelIcosahedron0() {
    rotate([0, 0, 0])  edgeIcosahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 120]) edgeIcosahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=5);
    rotate([0, 0, 240]) edgeIcosahedron(isOdd = 1, skewX = 0, skewY = 0, nbTeeth=5);
}

panelIcosahedron0();