module edgeIcosahedron (isOdd, skewX, skewY, nbTeeth=5) {
    isNegative = isOdd;
    tolerance = .10;
    h2 = 150;
    a = 100;
    //h3 = sqrt(1/2 - (cos(30)/2)^2); //height of the face (r of circumscribed circle is a/sqrt(2))
    toothWidth= (a-a*2/10) / 2 / nbTeeth*0.815; //tooth take up 80% of border width
    b = a / 2 * tan(30); //distance from face center to middle of face edge 
    h3 = 0.755; //(radius of inscribed circle, sqrt(3)/12*(3+sqrt(5))
//    skewZ = h3 / sin(30); //tan() of quarter face height / distance from edge to face center
    echo(b);
    skewMat =   [ [ 1, skewX, skewY,   0],
                  [ 0,     1, 0,   0],
                  [ 0,     0,     1,   0],
                  [ 0,     0,     0,   1]];
    difference () {
        union() {
            rotate([0,0,90]) cylinder(r1=a/(2*cos(30)), r2=0, h=a*h3, $fn=3);
            if (!isNegative) {
                for (i = [0:nbTeeth-1]) {
                    translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth-32.5, -b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,8,24.2]);
                }
            }
        }
        translate([0, 10,0]) rotate([0,0,90]) cylinder(r1=a/(2*cos(30)), r2=a/(2*cos(30)), h=a*h3, $fn=3);
        rotate([0,0,-30]) translate([a/6, 50,0]) rotate ([0,0,90]) cylinder(r1=100, r2=100, h=a*h3, $fn=3);
        rotate([0,0,30]) translate([-a/6, 50,0]) rotate ([0,0,90]) cylinder(r1=100, r2=100, h=a*h3, $fn=3);
        if (isNegative) {
                for (i = [0:nbTeeth-1]) {
                    translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth-32.5, -b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,8,24.2]);
            }
        }
    }
}

//edgeIcosahedron(isOdd = 0, skewX = 0, skewY = 0, nbTeeth=6);