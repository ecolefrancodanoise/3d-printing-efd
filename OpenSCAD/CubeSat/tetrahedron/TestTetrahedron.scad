include <FaceTetrahedron.scad>

d = 10;

s2 = 1/sqrt(2);
vertices = [
[-1,  0, -s2,],
[ 1,  0, -s2,],
[ 0, -1,  s2,],
[ 0,  1,  s2,]
];

//translations = -vertices;
translations = [
[1,0,0],
[-1, 0,0],
[0, 1,0],
[0,-1,0],
];

normals = vertices;

rotDir = [-1, -1, 1, 1];
alignRot = [30, -30, 0, 60];
flipRot = 180 * [1, 1, 0, 0];

neighbors = [
[3, 1, 2],
[3, 2, 0],
[3, 0, 1],
[0, 2, 1]
];

parities = [
[1, 1, 1],
[0, 1, 0],
[1, 0, 0],
[0, 0, 1]
];

for (i = [0:3]) {
   translate(d*translations[i])
       translate(-16.6 * normals[i])
    rotate(54.8 * cross([0,0,1], rotDir[i]*normals[i])) rotate(alignRot[i]*[0,0,1]) rotate(flipRot[i]*[0,1,0])
    faceTetrahedron(nbTeeth=1, relativeBorderWidth=0.191, index = i,
            translations=translations, normals=normals, neighbors=neighbors, parity=parities[i]);
}
