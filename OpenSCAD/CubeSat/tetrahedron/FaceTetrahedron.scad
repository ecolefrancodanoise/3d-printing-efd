include <EdgeTetrahedron.scad>

module faceTetrahedron(nbTeeth=5, relativeBorderWidth=0.1, index, translations, normals, neighbors, parity) {
    
        for (i = [0:2]) {
        neighbor = neighbors[index][i];
        skew = -(translations[index] - translations[neighbor]) * (cross(normals[index], normals[neighbor])) ;
            echo(skew);
        isOdd = parity[i];
        isNegative = skew < 0? isOdd : (1-isOdd);
        rotate([0,0,i*120])  edgeTetrahedron(isOdd = isOdd, isNegative = isNegative,
            skewX = skew * isNegative, skewY = -skew * (1-isNegative),
            relativeBorderWidth = relativeBorderWidth, nbTeeth=nbTeeth);
    }
}
