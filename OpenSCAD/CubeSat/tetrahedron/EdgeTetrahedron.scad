module edgeTetrahedron (isOdd, isNegative, skewX, skewY, relativeBorderWidth=0.1, nbTeeth=5) {
    tolerance = .10;
    a = 100;
    r = a/(2*cos(30)); //radius of the "cylinder" used to generate the face
    bw = a * relativeBorderWidth;
    h3 = sqrt(6)/3 - sqrt(3/8); //height of the quarter face: tetrahedron height - radius of circumscribed sphere
    c =  bw/tan(30); //distance from vertice to start of teeth range
    toothWidth= (a-2*c) / 2 / nbTeeth; //tooth takes up 80% of border width
    b = 1 / 2 * tan(30); //distance from face center to middle of face edge
    edgeHeight = bw * h3 / b;

    skewZ = h3 / sin(30); //tan() of quarter face height / distance from edge to face center
    skewMat =   [ [ 1, skewX, skewY,   0],
                  [ 0,     1, skewZ,   0],
                  [ 0,     0,     1,   0],
                  [ 0,     0,     0,   1]];
    difference () {
        union() {
            rotate([0,0,90]) cylinder(r1=r, r2=0, h=a*h3, $fn=3);
            if (!isNegative) {
                for (i = [0:nbTeeth-1]) {
                    translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth - a/2 +c, -a*b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,0.8*bw,edgeHeight-2]);
                }
            }
        }
        translate([0, bw,0]) rotate([0,0,90]) cylinder(r1=r, r2=r, h=a*h3, $fn=3);
        rotate([0,0,-30]) translate([a/6, 50,0]) rotate ([0,0,90]) cylinder(r1=a, r2=a, h=a*h3, $fn=3);
        rotate([0,0,30]) translate([-a/6, 50,0]) rotate ([0,0,90]) cylinder(r1=a, r2=a, h=a*h3, $fn=3);
        if (isNegative) {
                for (i = [0:nbTeeth-1]) {
                    translate([i*toothWidth*2-tolerance/2+isOdd*toothWidth-a/2+c, -a*b+2.5, 2]) multmatrix(skewMat) cube([toothWidth-tolerance,0.8*bw,edgeHeight-2]);
            }
        }
    }
}

//edgeTetrahedron(isOdd = 1, isNegative = 0, skewX = 0, skewY = -1.41421, relativeBorderWidth=0.1, nbTeeth=3);