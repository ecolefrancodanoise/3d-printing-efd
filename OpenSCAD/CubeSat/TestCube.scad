include <Face.scad>
include <CubeConfig1.scad>

d = 3;
for (i = [0:2]) {
    translate(d*translations[i]) translate(-50 * normals[i])
    rotate(90 * cross([0, 0, 1], normals[i])) rotate(180 * faceInward[i] * [1, 0, 0])
        face(relativeBorderWidth=0.1, nbTeeth=5, index = i,
            translations=translations, normals=normals,
            neighbors=neighbors, parity=parities[i]);
}
