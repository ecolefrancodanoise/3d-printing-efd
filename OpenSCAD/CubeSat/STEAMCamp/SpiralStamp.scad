include <Face.scad>
include <CubeConfig1.scad> //choose angle configuration file

relativeBorderWidth=0.181;
bw = 100 * relativeBorderWidth;
faceThickness = 2.0;
innerWidth = 100 * (1 - 2*relativeBorderWidth);
for (i = [0:1]) {
for (j = [0:2]) {
    index = 3*i+j;
    translate([110*i, 110*j, 0])
    union() {
        difference() {
            union() {
            face(relativeBorderWidth=relativeBorderWidth,
                faceThickness = faceThickness, tolerance = 0.4, nbTeeth=2, index = index,
                translations=translations, normals=normals,
                neighbors=neighbors, parity=parities[index]);
                translate([-innerWidth/2,-innerWidth/2,0]) cube([innerWidth,innerWidth,bw]);
            }

            translate([-25,-25,5]) cube([50,50,bw]);
        }
        scale([1,1,-1]) union() {
            linear_extrude(4) import("multispiral.svg");
            difference() {
                translate([-50,-50,0]) cube([100,100,4]); 
                translate([-45,-45,0]) cube([90,90,4]);
            }
        }
    }
}
}