include <Face.scad>
include <CubeConfig1.scad> //choose angle configuration file

relativeBorderWidth=0.181;
bw = 100 * relativeBorderWidth;
faceThickness = 2.0;
innerWidth = 100 * (1 - 2*relativeBorderWidth);
for (i = [0]) {
for (j = [0]) {
    index = 3*i+j;
    translate([110*i, 110*j, 0])
    difference() {
        union() {
        face(relativeBorderWidth=relativeBorderWidth,
            faceThickness = faceThickness, tolerance = 0.3, nbTeeth=2, index = index,
            translations=translations, normals=normals,
            neighbors=neighbors, parity=parities[index]);
            translate([-innerWidth/2,-innerWidth/2,0]) cube([innerWidth,innerWidth,bw]);
        }

//            translate([30,-7,0]) scale([-1,1,1]) #linear_extrude(2.5) text("pass", size=20);
        translate([-30,-30,5]) cube([60,60,bw]);

        linear_extrude(1.5) import("livetstrae.svg");

    }
}
}