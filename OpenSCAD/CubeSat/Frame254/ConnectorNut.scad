include <BOSL2/std.scad>
include <BOSL2/screws.scad>
include <FrameConfig.scad>

module halfNut () {
translate([0, 0, plateHeight/2]) screw(spec = screwSpec, length=plateHeight, bevel1=false, bevel2=false, blunt_start = false, head="none", $fn=40, thread=true, drive = "torx");
}

halfNut();
scale([1, 1, -1]) halfNut();