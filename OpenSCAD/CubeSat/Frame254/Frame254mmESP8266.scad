include <Frame254mmBase.scad>

difference() {
    Frame254mmBase();
    for (xPos = [0:1]) {
        for (yPos = [0:3]) {
            translate([-12, -4]) translate([xPos*2.54, yPos*2.54,0]) cube([2.7, 2.7, plateHeight], center = true);
        }
    }
}