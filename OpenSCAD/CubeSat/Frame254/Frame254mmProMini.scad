include <Frame254mmBase.scad>

difference() {
    Frame254mmBase();
    offset = 0.2;
    translate([0, 0, (-plateHeight + pcbHeight + offset)/2]) 
        cube([33.5 + 2 * tol, 18.4 + 2 * tol, pcbHeight + offset], center = true);
    #translate([33.5/2 - 3/2, 0, 0]) cube([3, 18.4 + 2 * tol, plateHeight], center = true);
    #translate([23.5/2 - 3/2, 0, 0]) cube([3, 18.4 + 2 * tol, plateHeight], center = true);

}