include <../../common/ProMiniHoles.scad>
include <Frame254mmBase.scad>

difference() {
    union() {
        Frame254mmBase();
        cube([width, 2.54*7.5, plateHeight], center = true);
    }
    translate([-6.5, 6.35, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-6.5, -6.35, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-6.5, 4.0, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-6.5, -4.0, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-29.5, 6.5, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-29.5, -6.35, 0]) cylinder(h=plateHeight,d=2, center=true);
    translate([-17.5, 0, -0.25]) cube([28.0, 17.8, 1.5], center = true);
    #translate([-32, 0, -0.4]) cube([2.0, 10, 1.2], center = true);
}