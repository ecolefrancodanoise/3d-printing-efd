include <BOSL2/std.scad>
include <BOSL2/screws.scad>
include <FrameConfig.scad>
//connectorlength = 10.76;


module pinBases() {
    translate([0, 25.0, 1])
    for (i = [-N/2:N/2-isOdd]) {
        translate([2.54*(i+0.5*isOdd), 0, 0]) 
        union() {
            rotate([-45, 0, 0]) union() {
                cube([2.54+tol, 2.54+tol, 2.8], center = true);
                cube([1.2, 1.2, 12], center = true);
            }
            translate([0, -3.5, -1.5]) cube([1.2, 3.5, 1.2], center = true);
        }
    }
}

module Frame254mmBase() {

    difference() {
        cube([width, width, plateHeight], center = true);
        for (i = [0:3]) {
            #rotate([0, 0, i*90]) pinBases();
        }
        for (xPos = [-15, 15]) { //unisynx holes
            for (yPos = [-15, 15]) { //unisynx holes
                sign = xPos * yPos / abs(xPos * yPos);
                scale ([1, 1, sign]) translate([xPos,yPos,0]) screw_hole(spec = screwSpec, length=2, bevel1=false, bevel2=false, blunt_start = false, head="none", $fn=40, thread=true);
            }
        }
        translate([-8, 13, 0]) linear_extrude(2) text(ver, size=6);
    }        
}
