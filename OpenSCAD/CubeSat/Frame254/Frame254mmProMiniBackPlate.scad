include <Frame254mmBackplate.scad>

difference() {
    Frame254mmBackplate();
    #translate([33.5/2 - 3/2, 0, 0]) cube([3, 17, plateHeight], center = true);
    #translate([-11.7, 0, 0]) cube([4, 6.5, plateHeight], center = true);
    #translate([33.5/2-1.5-8.1, 4.4, 0]) cube([5, 3, plateHeight], center = true);
    #translate([33.5/2-1.5-8.1, -4.4, 0]) cube([5, 3, plateHeight], center = true);
}