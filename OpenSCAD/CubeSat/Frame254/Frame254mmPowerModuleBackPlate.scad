include <../../common/ProMiniHoles.scad>
include <Frame254mmBackplate.scad>

difference() {
    Frame254mmBackplate();
    translate([30, 0, -0.25]) cube([5.0, 10, 1.5], center = true);
}