include <BOSL2/std.scad>
include <BOSL2/screws.scad>
include <FrameConfig.scad>


difference() {
    union() {
        difference() {
            cube([width, width, plateHeight], center = true);
            #translate([0, 0, -plateHeight/4]) cube([width-plateHeight*2, width-plateHeight*2, plateHeight/2], center = true);
        }
        for (xPos = [-15, 15]) { //unisynx holes
            for (yPos = [-15, 15]) { //unisynx holes
                translate([xPos,yPos,0]) cylinder(h=plateHeight, d=9.5, center = true, $fn=60);
            }
        }
    }
    for (xPos = [-15, 15]) { //unisynx holes
        for (yPos = [-15, 15]) { //unisynx holes
            sign = xPos * yPos / abs(xPos * yPos);
            scale ([1, 1, sign]) translate([xPos,yPos,0]) screw_hole(spec = screwSpec, length=2, bevel1=false, bevel2=false, blunt_start = false, head="none", $fn=40, thread=true);
        }
    }
    translate([-8, 13, 0.5]) linear_extrude(0.5) text(ver, size=6);
}