include <Container_Values.scad>;
$fn=100;
difference(){
    union(){
    cylinder(d=DiaMeter+WallThikness*2,h=HeighT+BottomThikness);
    for (i=[1:LockNumber]){  
rotate([0,0,(360/LockNumber)*i])translate([(DiaMeter+WallThikness*2)/2-0.3,0,HeighT+BottomThikness-LidHeighT/2])sphere(d=LockSize);  
}
}
    translate([0,0,BottomThikness])cylinder(d=DiaMeter,h=HeighT+BottomThikness);
}
