$fn=64;
MonsterName="Bandit";
MonsterNumber=11;
EliteMonster=0; //1 for Elite, 0 for none Elite
align=[ "left","center","right" ];
difference(){
cylinder(d=25,h=2.5);
translate([0,-2,1])linear_extrude(height = 2) {
text(MonsterName,font="Liberation Sans:style=Bold", size = 4.5,halign=align[1]);
}
    translate([0,-10,1])linear_extrude(height = 2) {
text(str(MonsterNumber),font="Liberation Sans:style=Bold", size = 6,halign=align[1]);
    }
if(EliteMonster==1){
    translate([0,0,1])linear_extrude(height = 2) {
text("*",font="Liberation Sans:style=Bold", size = 10,halign=align[1]);
}
}
}