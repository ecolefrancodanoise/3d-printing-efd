$fn=120;
tolerance =0.1;
union() {
    difference(){
        cube([32,64,10], center=true);
        translate([0,0,2])cube([20,41,7], center=true);
        translate([0,0,2])cube([7,64,7], center=true);
        translate([0,0,-5])cube([32 - 1.2 * 2,64 -1.2 * 2,(1.6+tolerance)*2], center=true);  
    }
    for (i =[ -1:1:1 ]) {
        for (j = [ -3:1:3 ]) {
            translate([i * 8, j * 8, -5]) difference() {
                cylinder (d=6.51-tolerance,h=1.6+tolerance);
                cylinder (d=4.8+tolerance,h=1.6+tolerance);
            }
        }
    }
}