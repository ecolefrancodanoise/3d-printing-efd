// Simple custom polybowl

radius = 19;
sides = 20;
thickness =2;
bodyheight=50;
bodytwist=150;
bodyflare=2;
baseheight=5;
rimheight=5;

///Renders

//base
linear_extrude(height=baseheight)
Polyshape(solid="yes");

//body
translate([0,0,baseheight])linear_extrude(height=bodyheight, twist=bodytwist, scale=bodyflare, slices=2*bodyheight)
Polyshape(solid="no");

//rim
translate([0,0,baseheight+bodyheight])rotate(-bodytwist)
scale(bodyflare)
linear_extrude(height=rimheight)
Polyshape(solid="no");

//////////// Modules

module Polyshape(solid){
difference(){
    // outside shape
    offset( r=5, $fn=48 )
    circle(r=radius, $fn=sides);
   // minus inside shape
   if (solid=="no"){
    offset( r=5-thickness, $fn=48)
    circle( r=radius, $fn=sides);
   }
   }
   }