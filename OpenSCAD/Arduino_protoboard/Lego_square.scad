$fn=64;
length=2;
width=2;
cylinder_size=4.9;
difference(){
cube([8*length-0.2,8*width-0.2,3.2]);
for(j = [8:10:(8*(length))]){
    for(i = [8:10:(8*(width))]){
translate([j,i,0])cylinder(d=3.5,h=1.7+3.2);
    }
}
}
for(j = [4:8:(8*length)]){
    for(i = [4:8:(8*width)]){
translate([j,i,0])cylinder(d=cylinder_size,h=1.7+3.2);
    }
}