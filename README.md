Models for 3D printing made at the Danish-French School of Copenhagen

Please only add source files (e.g. .scad, .blender, sometimes even .stl) and please no large gcode or rendered files.
