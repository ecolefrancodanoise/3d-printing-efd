$fn = 48;

translate([0,0,3])
{
    difference() {
        // Hollow cylinder
        cylinder(h=10, d=21+2*1.70);
        translate([0,0,-.009]) cylinder(h=10+2*.009, d=21);
        // Half cut
        translate([-20,3,-.009]) cube([40,20,10+2*.009]);
    }
    // Rounded ends
    r = (10.5+1.70/2)*cos(atan(3/(10.5+1.70/2)));
    translate([+r,3,0]) cylinder(h=10, d=1.70+0.5);
    translate([-r,3,0]) cylinder(h=10, d=1.70+0.5);
}

difference() {
    // Back
    cylinder(h=3, d=21+2*1.70+2*1.70);
    // Screw hole
    translate([0,3,0]) {
        translate([0,0,2.5-.009]) cylinder(h=0.5+2*.009, d=4.5*2);
        translate([0,0,0.5])
        rotate_extrude() {
            polygon([[0,0],[2.0,0],[4.5,2],[0,2]]);
        }
        translate([0,0,-.009]) cylinder(h=0.5+2*.009, d=2.0*2);
    }
}
