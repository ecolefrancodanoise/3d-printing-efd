$fn=64;
fraction=12; 
// 2=Black, 3=Grey, 4=White, 5=Green 6=Blue, 7=Transparent, 8=Red, 10=orange, 12=Purple.

translate([0,0,0]) sector(6, 96, (360/fraction));
module sector(h, d, a2) {
difference() {
            cylinder(h=h, d=d);
            rotate([0,0,0]) translate([-d/2, -d/2, -0.5])
                cube([d, d/2, h+1]);
            rotate([0,0,a2]) translate([-d/2, 0, -0.5])
                cube([d, d/2, h+1]);
}
difference() {
            cylinder(h=2, d=100);
            rotate([0,0,0]) translate([-(100)/2, -(100)/2, -0.5])
                cube([(100), (100)/2, h+1]);
            rotate([0,0,a2]) translate([-(100)/2, 0, -0.5])
                cube([(100), (100)/2, h+1]);
}

}